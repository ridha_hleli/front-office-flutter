
import 'package:flutter/widgets.dart';
import 'package:salesTeamFront/Screens/dashborad/dashboard.dart';
import 'package:salesTeamFront/Screens/login/login.dart';


final Map<String , WidgetBuilder> routes = {
  LoginScreen.routeName : (context) => LoginScreen(),
  DashboardScreen.routeName : (context) => DashboardScreen(),
};
