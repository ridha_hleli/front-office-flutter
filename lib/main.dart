import 'package:flutter/material.dart';
import 'package:salesTeamFront/Screens/login/login.dart';
import 'package:salesTeamFront/routes.dart';

import 'Screens/login/login.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Sales Teams',
      theme: ThemeData(
        
        
      ),
      initialRoute: LoginScreen.routeName,
      routes: routes,
      
      
    );
  }
}
